# QPAML

## Original code

To access the original code of the manuscrit, please visit the following link [realese 0.0.1](https://gitlab.com/amalib/qpaml/-/releases/0.0.1-pre).

## Description

This code aims to classify the impact of metabolic reactions on the production of specific metabolites, utilizing Flux Balance Analysis, Qualitative Perturbations Analysis, and Gradient Boosting Decision Trees.

## Getting started

The scripts have been tested on Windows using Python 3.8.0 and specified versions of pip packages. Note that altering these versions may lead to errors or unsuccessful installations.

The execution time for L-tryptophan production on a laptop with an Intel i5-11400H and 8 GB of RAM is approximately 5 minutes.

## Files and directories

**QPAML:** Main directory.

&emsp;**/readme.md:** This file.

&emsp;**/biocyc/:** Directory containg flat files of EcoCyc for the regulation of _E. coli_.

&emsp;**/dist/:** Installation packages for pip.

&emsp;**/src/:** Code and data files.

&emsp;**/LICENSE:** License file for Apache 2.0.

&emsp;**/MANIFEST.in:** Specify the files required for creation of installer.

&emsp;**/iML1515a_QPAML.xml:** Modificated version of the iML1515a model used in the publication.

&emsp;**/setup.py:** Configuration file used for setuptools.

## Installation
**Windows:**

1.- Install [python 3.8.0](https://www.python.org/downloads/release/python-380/).

2.- Create a main directory where you can create a virtual environment.
```
>mkdir QPAML
```
3.- Access the QPAML directory.
```
>cd QPAML
```
4.- Create a virtual environment, ensuring the use of Python 3.8.0. If you have other Python versions installed, you can access python 3.8.0 using the following code.

Replace "**USERNAME**" with the user of your local machine.
```
>C:\Users\USERNAME\AppData\Local\Programs\Python\Python38\python.exe -m venv .
```
5.- Activate the virtual environment.
```
>cd Scripts
>activate
```
If the virtual environment is activated, each line should start with "(QPAML)".
```
(QPAML)>cd ..
```
6.- Install QPAML package using pip.
```
(QPAML)>pip install --upgrade pip
(QPAML)>pip install QPAML
```


## Usage

Ensure that you are within the QPAML folder and have activated the virtual environment before executing the following command.

1.- Access to python
```
(QPAML)>python
```
2.- Import libraries
```
>>>import QPAML
>>>from cameo import load_model
>>>from QPAML.core import QPAML
```
3.- Load model.
```
>>>tmodel = load_model('./iML1515a_QPAML.xml')
```
4.- Create an instance of QPAML package.
```
>>>qpaml = QPAML(tmodel)
```
5.- Define the unrestricted media for D-glucosa consumption.
```
>>>qpaml.constrain_unrestricted_media('EX_glc__D_e', lb = -18.0)
```
6.- Define the restricted media and growth conditions.
```
>>>qpaml.constrain_restricted_media('EX_glc__D_e', lb = -16.0)
>>>qpaml.constrain_restricted_media('EX_o2_e', lb = -7.5)
>>>qpaml.constrain_restricted_media('EX_co2_e', lb = 7.0, ub = 1000)
>>>qpaml.constrain_restricted_media('BIOMASS_Ec_iML1515_core_75p37M', lb = 0.05)
```
7.- Run classification algorithm.
```
>>>qpaml_res = qpaml.run(target='EX_trp__L_e', biomass='BIOMASS_Ec_iML1515_core_75p37M')
>>>qpaml_res.predicted
```
8.- Calculate the distances from pFBA optima reactions.
```
>>>avoid = ['h', 'h2o', 'nadp', 'nad', 'atp', 'pi', 'ppi', 'nadph', 'nadh', 'coa', 'co2', 'amp', 'adp', 'nh4', 'o2', 'so4', 'gdp', 'gtp', 'utp', 'udp', 'ump']
>>>qpaml_res.update_distances(avoid)
>>>qpaml_res.disruption_distance
```
9.- Find reactions regulated by negative feedback.
```
>>>avoid_regulators = ['ATP', 'ADP', 'AMP', 'NAD', 'NADH', 'NADP', 'NADPH', 'Pi']
>>>regulated_reactions = qpaml_res.find_fb_rn(tmodel, avoid_regulators, './biocyc/')
```


## Support
Please do not hesitate to share your inquiries regarding the code and interpretation of results with [miguel.ramos@cinvestav.mx](mailto:miguel.ramos@cinvestav.mx).

## Issues
Report any issue into [issues section](https://gitlab.com/amalib/qpaml/-/issues)


## Roadmap
- An enhancement to QPAML could involve incorporating annotations for additional metabolites. 
- The possibility of integrating regulatory levels remains a prospective consideration for future implementation.

## Contributing
Feel free to provide any suggestions to [agustino.martinez@cinvestav.mx](mailto:agustino.martinez@cinvestav.mx). Your input is appreciated.

## Authors
- [Miguel Angel Ramos Valdovinos](https://github.com/marv123456).
- Prisciluis Caheri Salas Navarrete.
- Gerardo R. Amores.
- Ana Lilia Hernandez Orihuela.
- Agustino Martinez Antonio.

## License
Copyright 2024 Laboratorio de Ingenieria Biologica, Departamento de Ingenieria Genetica, CINVESTAV Unidad Irapuato

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Citing
Ramos-Valdovinos, M.A.; Salas-Navarrete, P.C.; Amores, G.R.; Hernández-Orihuela, A.L.; Martínez-Antonio, A. Qualitative Perturbation Analysis and Machine Learning: Elucidating Bacterial Optimization of Tryptophan Production. Algorithms 2024, 17, 282. https://doi.org/10.3390/a17070282

```
@Article{a17070282,
AUTHOR = {Ramos-Valdovinos, Miguel Angel and Salas-Navarrete, Prisciluis Caheri and Amores, Gerardo R. and Hernández-Orihuela, Ana Lilia and Martínez-Antonio, Agustino},
TITLE = {Qualitative Perturbation Analysis and Machine Learning: Elucidating Bacterial Optimization of Tryptophan Production},
JOURNAL = {Algorithms},
VOLUME = {17},
YEAR = {2024},
NUMBER = {7},
ARTICLE-NUMBER = {282},
URL = {https://www.mdpi.com/1999-4893/17/7/282},
ISSN = {1999-4893},
ABSTRACT = {L-tryptophan is an essential amino acid widely used in the pharmaceutical and feed industries. Enhancing its production in microorganisms necessitates activating and inactivating specific genes to direct more resources toward its synthesis. In this study, we developed a classification model based on Qualitative Perturbation Analysis and Machine Learning (QPAML). The model uses pFBA to obtain optimal reactions for tryptophan production and FSEOF to introduce perturbations on fluxes of the optima reactions while registering all changes over the iML1515a Genome-Scale Metabolic Network model. The altered reaction fluxes and their relationship with tryptophan and biomass production are translated to qualitative variables classified with GBDT. In the end, groups of enzymatic reactions are predicted to be deleted, overexpressed, or attenuated for tryptophan and 30 other metabolites in E. coli with a 92.34% F1-Score. The QPAML model can integrate diverse data types, promising improved predictions and the discovery of complex patterns in microbial metabolic engineering. It has broad potential applications and offers valuable insights for optimizing microbial production in biotechnology.},
DOI = {10.3390/a17070282}
}
```



